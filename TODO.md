

jouter l'exemple d'un volume NFS 

# Volume NFS

``` yaml
app:
    image: ...
    ...
    volumes:
      - websitefiles:/u02/www



volumes:
  websitefiles:
    driver: local
    driver_opts:
      type: "nfs"
      o: "addr=<NFS_HOST>,nolock,soft,rw"
      device: ":<PATH>"
```