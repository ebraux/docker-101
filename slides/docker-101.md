---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->
![bg left:30% 85%](img/docker.png)


# Docker 101

### Principes et concepts de base

-emmanuel.braux@imt-atlantique.fr-

---

# Licence informations


Auteur : -emmanuel.braux@imt-atlantique.fr-

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 70%](img/docker.png)

# Docker 101

- **Origine de Docker**
- **L'architecture de Docker**
- **Docker Compose**
- **Docker Swarm**
- **Bonnes pratiques**

---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 70%](img/docker.png)

# Docker 101

## > Origine de Docker
- **L'architecture de Docker**
- **Docker Compose**
- **Docker Swarm**
- **Bonnes pratiques**

---
# Définition de Docker

**Docker** est une plateforme permettant de lancer des applications dans des conteneurs.
...
Un conteneur permet d'isoler les uns des autres, des services s’exécutant sur un OS 
...  On parle de virtualisation d'OS

*Source : [wikipédia](https://www.wikipedia.org/wiki/Docker_(logiciel))*

---
# Baremetal vs Virtualisation vs conteneur

 ![h:500 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/evolution-virtualization.png)

*Image source [https://medium.com/](https://medium.com/)*


---
# Conteneur vs VM

## Avantages :
-  Plus léger et plus petit.
 - Meilleure utilisation des ressources
   
## Inconvénients : 
- Dépendant du noyau de la machine hôte
- Moins bonne "isolation", risque de sécurité.

> VM = isolation de bas niveau  

---
# Origine

Solomon Hykes en 2013 : 

Besoin d'une solution **simple et efficace**, permettant aux **développeurs** de disposer d'un environnement technique, ou de tester une application :
- **Portable** : différents postes de travail
- **Partageable** : travail en équipe
- **Reproductible** : besoin de cycle dev/prod, ...
- **Archivable** : gestion de configuration, ...
- **Consommant peu de ressources** : pour fonctionner les postes des développeurs

---

# Quelles solutions avant Docker

- Installation de **stacks applicatifs complets** (Lamp, ...) : 
  - Complexe, nécessite des compétences techniques,
  - Difficile à maintenir 
- Utilisation de **machines virtuelles** :
  - Gros fichiers,
  - Consommation de ressources,
  - Complexe à faire évoluer/adapter
- Utilisation de **conteneurs** (LXC, OpenVZ, ...):
  - Réservé aux ingénieurs système Linux 

---
# Initiatives équivalentes à Docker

  - **Appliances** (VM pré-packagées)
    - Toujours les inconvénients des VMs
  - **Vagrant** : gestion de déploiement de VMs depuis un fichier de configuration
    - Toujours une approche VM
    - Reste complexe à prendre en main et à utiliser

---
# La réponse de Docker

- Une **commande unique** pour tout gérer
- Un **catalogue d'applications pré-packagées**  (les images)
  - "Légères", personnalisables
  - Orientées "application"
  
- Un **fichier de personnalisation** simple et puissant (Dockerfile)
- Des **environnements d'exécution** (conteneurs)
  - Légers,
  - Multi-plateforme 

---

# Approche orientée usage : masquer la technique

## Utiliser Docker, c'est choisir une application, et la lancer.
## ... et c'est tout.

### (ou presque)


---
# L'histoire de Docker

![h:500 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/History-of-Docker.png)

*crédits [www.slideshare.net](www.slideshare.net)*

---
# Adoption de Docker

## Adoption très rapide par les développeurs (DEV) :

- Images disponibles pour la plupart des applications
- Intégration dans les IDE,
- Des clients graphiques, une API complète.

## Adoption très lente par les administrateurs systèmes (OPS) :
- Ca vient du monde des DEVs
- C'est arrivé très vite, pas prévu pour du passage en prod
- Des gros problèmes de sécurité, au moins au début
  
---

# Docker a accompagné l'evolution des architectures applicatives - Microservices

![h:450 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/monolith-to-microservices.png)


---
# L'écosystème Docker

![h:500 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-ecosystem.png)

*Image source [http://www.docker.com/](http://www.docker.com/)*

---
# Pour résumer Docker c'est quoi ?

## Une solution simple pour **packager** et **déployer** des applications

- Une solution de **conteneurisation d'applications**
- Une sur-couche au dessus de **technologies existantes**, pour les rendre **simples**
- Opensource : large **communauté**
- Version "**Enterprise**", avec support


## **Révolutionnaire d'un point de vue usage !!**

---
# A ce jour 

- Devenu **incontournable** dans les développements.

- Fer de lance du mouvement **DevOps**.

Pour la production : utilisation d'**orchestrateurs** (Swarm, Kubernetes, ...)

Des alternatives émergent : podman, cri-o, ...

---
# Un modèle économique qui se cherche

La société Docker s'est recentrée sur le développement : 

- **Docker** : environnement de dev (Docker cli, Docker UI et registry)
- **Mirantis** : Docker conteneur Engine

Des limitations qui apparaissent :
- Limitation des accès au catalogue d'images, ...

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Premiers pas" : Utilisation de Docker

- Lancer un serveur web en local
- Générer un mot de passe au format "htpasswd"

---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)


# Docker 101

- ***Origine de Docker***
## > L'architecture de Docker
**Les conteneurs**
    *Les images, Les Dockerfile, Les registries, Les Volumes, Les réseaux*
- **Docker Compose**
- **Docker Swarm**
- **Bonnes pratiques**

---
# Docker

![bg right:60% 95% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-architecture.png)

- Modèle client/serveur
- Multi-services
- Des services externes


*Image Source Credits: [Docker](http://www.docker.com/)*

---
# Les images

![bg left:40% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-filesystems-multilayer.png)

- Système de fichier **statique**

- Contient le code de l'application, les librairies, les binaires et runtimes, la configuration.




*Image source [Docker](https://docs.docker.com/)*

---
# Les conteneurs

![bg left:30% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/container-logo.png)

- Instanciation d’une image

- **Dynamique**, Léger 

- L'image contient des informations système pour lancer le conteneur  

*Image source [Docker](https://docs.docker.com/)*


---
# Les Registry

![bg left:35% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/dockerhub.png)

- Catalogue d'application pré-packagées

- API REST

- Principalement : Docker HUB

- De nombreuses autres Registries

*Image source [Docker](https://docs.docker.com/)*

---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)


# Docker 101

- ***Origine de Docker***
## > L'architecture de Docker
**Les Conteneurs**
    *Images, Dockerfiles, Registries, Volumes, Réseaux*
- **Docker Compose**
- **Docker Swarm**
- **Bonnes pratiques**



---
# Les conteneurs

![bg left:30% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/container-archi.png)

- Un conteneur est autonome.
- Il partage le système d'exploitation de l'hôte (noyau), et n'en fournit aucun.
- Indépendant de la configuration et des applications de l"hôte"
- Instanciation d’une image

  
*Image source [Docker](https://docs.docker.com/)*

---
# VM vs conteneurs

 ![h:450 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/vm-vs-container.png)

*Image source [Docker](https://docs.docker.com/)*

---
# Commandes pour gérer les conteneurs

`docker container COMMAND`

- `ps` :  lister les conteneurs
- `run` : lancer un conteneur
- `exec` : exécuter une commande dans un conteneur existant
- `stop/start` : arrêter/démarrer un conteneur
- `rm` :  supprimer un conteneur
- `help`, `inspect`, ...
  
> Rem : '**container**' est optionnel dans la ligne de commande

*[https://docs.docker.com/engine/reference/commandline/container/](https://docs.docker.com/engine/reference/commandline/container/)*

---
# Principales options de lancement d'un conteneur
`docker run [OPTIONS] IMAGE [COMMAND] [ARG...]`

- `-d` : lancement en mode daemon
- `-it` : lancement en mode interractif
- `-p  HOST_PORT:CONTAINER_PORT` : publier un port du conteneur sur un port de l'hôte  
- `--name <nom>` : donner un nom au conteneur
- `--rm` : supprimer le conteneur après son execution

*[https://docs.docker.com/engine/reference/commandline/run/](https://docs.docker.com/engine/reference/commandline/run/)*

---
# Remarques concernant les conteneurs

 - Les conteneurs sont conçus pour n'exécuter qu'**une seule commande**
 - Une fois la commande terminée, les conteneurs s'arrêtent
 - Les commandes dans un conteneur ne s’exécutent pas en tâche de fond: **pas de mécanisme de type systemd.**
 - Les commandes sont limitées à celles présentes dans le conteneurs : **pas d’accès aux commandes de l’hôte**
- On ne se connecte **pas en SSH** à un conteneur, mais avec `docker exec -it ... ` 

---

<!-- _class: invert -->
<!-- _color: white -->

# LABs :

- Lancer un conteneur
- Lancer une commande
- Le mode détaché
- Le mode interactif
- Accès réseau

---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)


# Docker 101

- ***Origine de Docker***
## > L'architecture de Docker
**Les Images**
    *Conteneurs, Dockerfiles, Registries, Volumes,  Réseaux*
- **Docker Compose**
- **Docker Swarm**
- **Bonnes pratiques**

---
# Les images

![bg left:40% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-filesystems-multilayer.png)

- Constituées de plusieurs couches. 
- Mise à plat = le système de fichier racine des conteneurs
- Le code de l'application, les librairies, les binaires et runtimes, la configuration...

*Image source [Docker](https://docs.docker.com/)*

---
# Images et conteneurs

![bg left:40% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/image-multi.png)

**Chaque image est construite une seule fois​**

On peut construire N conteneurs à partir d'une image.

Un conteneur : 
  - Toutes les couches sont en lecture seule​
  - Une couche supplémentaire accessible en écriture, propre au conteneur​

*Image source [Docker](https://docs.docker.com/)*

---
# Images ... et Images

![bg left:35% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/image-layer-sharing.png)

Une image peut servir de base à la construction d’une autre image​.

Les images peuvent partager des couches :
- Lecture seule
- Copy-On-Write (CoW) ​

**Pas de duplication des données !!!**

​


*Image source [Docker](https://docs.docker.com/)*

---
# Remarques concernant les images

- Économie de place: les couches de l’image ne sont pas **dupliquées**.
- Un conteneur qui **écrit** beaucoup prend plus de place qu’un conteneur qui ne fait que de la lecture​
- Les conteneurs démarrent très **rapidement** : la seule opération est la création de la couche supérieure.
- Les données sont supprimées en **même temps** que les conteneurs (couche en R/W)

---
# Utilisation des images

## Téléchargées depuis une registry 
- Ne nécessite aucune opération complémentaire

## Personnalisation :
- A partir d'une image  existante
- Personnalisation via un fichier texte : **Dockerfile**
- Création d'une nouvelle image : opération de **Build** 

### Grande souplesse pour la personnalisation, contrairement aux VMs.

---
# Identification d'une image

### Format : `<REGISTRY>`**/**`<REPOSITORY>`**:**`<TAG>`

- `REGISTRY` : 
  - Catalogue auquel se connecter, par défaut le  DockerHub.
- `REPOSITORY` :
  -  Ensemble d’images similaires, portant des tags différents​
  - *Usage : Repository = nom de l’image*
  - Exemple : Php​ / Mysql​ / Python​/ ...
- `TAG` : 
  - Identifiant d'une variante d'une image
  - *Usage : Tag = Version de l'image*

---
# Les tags


- Le tag par défaut est "latest"
  - Attention ce n'est pas forcément la dernière version d'une application
  - C'est le mainteneur de l'image qui l'affecte
  - Son utilisation est a éviter

- Pour appliquer un tag à une image : 
`docker tag my-http my-registry:5000/my-company/httpd:v1.0`

---
# Commandes pour gérer les images

`docker image COMMAND`

- `ls` :  liste les images disponibles sur l'hôte
- `pull` : copier en local une image depuis une registry
- `rm` : supprimer une image 
- `help`, `inspect`, `push`, ...


*[https://docs.docker.com/engine/reference/commandline/image/](https://docs.docker.com/engine/reference/commandline/image/)*

---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Gestion des images"

- Manipuler des images
- Observer la gestion des tags

---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)


# Docker 101

- ***Origine de Docker***
## > L'architecture de Docker
**Les Dockerfile**
    *Conteneurs, Images, Registries, Volumes, Réseaux*
- **Docker Compose**
- **Docker Swarm**
- **Bonnes pratiques**

---

# Dockerfile

- Ensemble d'instructions pour construire une image 
- Fichier texte
  - Description / documentation 
  - Gestion de version
- Des commandes prédéfinies
- Utilisation de commandes shell standards

---
# Principe

![ drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/image-workflow.jpg)

*Image source [Docker](https://docs.docker.com/)*

---
# Principales commandes dans un fichier Dockerfile

- **FROM** : spécifie l'image de base
- **RUN** : exécution d'une commande shell
- **COPY** : copie d'un fichier depuis le système local, vers l'image
- **ENV** : définition de variables d'environnement
- **WORKDIR** : modification du chemin courant
- **CMD** : définition de la commande à exécuter au lancement du conteneur

*[https://docs.docker.com/engine/reference/builder/](https://docs.docker.com/engine/reference/builder/)*

---
## Exemple simple de Dockerfile

```dockerfile
FROM ubuntu:20.04       <-- image de base Ubuntu

COPY . /app             <-- copie du contenu du dossier
                            courant dans l'image

RUN make /app           <-- lancement de la commande make
                             pour générer l'application

WORKDIR /app            <-- modification du chemin courant

CMD python app.py       <-- configuration de la commande 
                            a exécuter par le conteneur
```
---
# Processus de build

- `FROM ubuntu:20.04` : 
  - Création du "contexte de build", en surcouche de l'image
- `COPY . /app ` : 
  - Copie du contenu du dossier courant dans `/app` dans le contexte de build
- `RUN make /app` : 
  - Lancement d'un conteneur intermédiaire, et exécution de la commande `make`
  - Création d'une couche R/O pour l'image, à partir de de la couche courante du conteneur.
  - Suppression du conteneur temporaire
- `WORKDIR /app` et `CMD python app.py`: modification des métadonnées de l'image

  

---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Personnaliser une image"
- Créer un DockerFile
- Builder une image
- Expérimenter le fonctionnement de WORKDIR
  
---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)


# Docker 101

- ***Origine de Docker***
## > L'architecture de Docker
**Les Registries**
    *Conteneurs, Images, Dockerfile,Volumes, Réseaux*
- **Docker Compose**
- **Docker Swarm**
- **Bonnes pratiques**
    
---

# La registry Docker

## Un catalogue d'applications pré-packagées
- Images "officielles" Docker
- Images "officielles" d'éditeurs de solutions
- Images proposées par des contributeurs

> Attention aux fausses images "officielles" : Docker ne gère pas la propriété des noms de projet.

---
# Les autres registry

- Sociétés spécialisés dans la création d'images, avec valeur ajoutée (sécurité, stabilité, ...). Ex : Quays.io, Jfrog.com, ...

- Registry interne / d'entreprise : gérée par votre "écosystème".
- Votre propre repository : très facile à créer, il suffit de lancer une image fournie par Docker.

---

# Détail du processus conteneur / image

Le lancement d'un conteneur se fait depuis une image stockée en local.

Si utilisation d'une image distance (registry) :
- Première étape = récupération (pull) depuis la registry
- Puis intégration au dépôt local.

Si construction depuis un Dockerfile :
- Première étape = récupération de l'image de base, 
- Construction de image (build) et intégration au dépôt local.
- *[optionnel]* Intégration à une registry distante pour réutilisation (push)

---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Le Docker HUB" : 
## [https://hub.docker.com/](https://hub.docker.com/)
- Parcourir le Docker Hub: "Explore"
- Se créer un compte : "Sign Up" (optionnel)

---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)


# Docker 101

- ***Origine de Docker***
## > L'architecture de Docker
**Les Volumes**
    *Conteneurs, Images,  Dockerfile, Registries, Réseaux*
- **Docker Compose**
- **Docker Swarm**
- **Bonnes pratiques**

---
# Les volumes

Les données dans les conteneurs ne sont pas **persistantes**
- Suppression d'un conteneur  = suppression des données.
- Pour la persistance des données : utilisation des volumes

---
# 2 types de volumes :

**Volumes "montés" : lien direct vers le filesystem local**
- Simple à utiliser 
- Fonctionnalités limitées
- A privilégier pour un usage en développement
  
**Volumes docker : object Docker natif**
- Gestion intégrée à la commande Docker
- Nombreuses options
- Fonctionnalités avancées (notament en mode cluster)

---
# Volume drivers

- Mécanisme de stockage OverlayFS par défaut :
  - Driver "overlay2", stockage sur le disque local.

- Drivers spécifiques : TMPFS, SSHFS, NFS, CIFS, ...

- Drivers proposés par les fournisseurs de solutions de stockage
  - Logiciels : GlusterFS, BeeGFS, ...
  - Matériels :  NetApp, HPE 3PAR, ...
  - Cloud : Azure, DigitalOcean, ...
  

*[https://docs.docker.com/storage/volumes/#use-a-volume-driver](https://docs.docker.com/storage/volumes/#use-a-volume-driver)*
*[https://docs.docker.com/engine/extend/legacy_plugins/#volume-plugins](https://docs.docker.com/engine/extend/legacy_plugins/#volume-plugins)*

---
# Commandes pour gérer les Volumes

`docker volume COMMAND`

- `ls` :  liste les volumes existants
- `create` : créer un volume
- `create --driver` : créer un volume avec un driver spécifique
- `rm` : supprimer un volume
- `help`, `inspect`,  ...


*[https://docs.docker.com/engine/reference/commandline/volume/](https://docs.docker.com/engine/reference/commandline/volume/)*

---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Données persistantes" : 
- Le contenu d'un conteneur est éphémère
- Conserver des données dans un volume

---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)


# Docker 101

- ***Origine de Docker***
## > L'architecture de Docker
**Les réseaux**
    *Conteneurs, Images, Dockerfile, Registries, Volumes*
- **Docker Compose**
- **Docker Swarm**
- **Bonnes pratiques**


---
# Les réseaux

Par défaut :
- Chaque conteneur dispose d'une adresse IP propre, différente de celle de l'hôte.
- Par défaut, ils sont tous connectés au même réseau ("default").
- Ils peuvent échanger entre eux.
- Ils ne sont pas accessibles directement depuis l'extérieur.
- Pour les rendre accessibles, il faut partager les ports avec la machine hôte
  
  
## **Il est possible de créer plusieurs réseaux.**

---
# Network Drivers

- **Bridge :**
  - Type de réseau utilisé pour "default", et par défaut.
  - Permet un accès extérieur via un mécanisme de redirection ports de la machine hôte (NAT).
- **None :**
  -  Aucune connexion réseau (sauf loopback)
- **Host :** 
  - Utilise la même interface que la machine hôte, et la même adresse IP
  - Les conteneurs sont accessibles depuis l'extérieur
  - Limitation dans l'usage des ports

---
# Network Drivers

- **Overlay :**
  - Réseau distribué entre plusieurs hôtes
  - Utilisé lors de la mise en cluster de Docker (Swarm)

- **Drivers externes**
  - Sous forme de plugins, ou d'intégration parfois complexes
  - Weave Network, Calico, Flannel, ...
  - [https://docs.docker.com/engine/extend/legacy_plugins/#network-plugins](https://docs.docker.com/engine/extend/legacy_plugins/#network-plugins)

---
# Network Drivers : utilisation de réseaux existants

- **Utiliser des réseaux définis en dehors de Docker (Vlan, subnets, ...).**
  - Ne pas devoir adapter la configuration existante à l'usage de docker
  - Garder le contrôle sur la configuration réseau
  - Eviter l'*overhead* apporté par la virtualisation de réseau

- **MacVlan :**
  -  Les interfaces réseau des conteneurs sont vues comme des interfaces physiques de la machine hôte, et disposent d'une adresse MAC
- **IPVlan:**
  - Les conteneurs disposent d'une adresse IP sur le même réseau que l'hôte

---
# Commandes pour gérer les Réseaux

`docker network COMMAND`

- `ls` :  liste les réseaux existants
- `create` : créer un réseau
- `create --driver` : créer un réseau avec un driver spécifique
- `rm` : supprimer un réseau
- `connect`, `disconnect` : connecte/déconnecte un conteneur existant à un réseau
- `help`, `inspect`,  ...

*[https://docs.docker.com/engine/reference/commandline/network/](https://docs.docker.com/engine/reference/commandline/network/)*

  
---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "Réséau"
- Utiliser le réseau "Default"
- Créer un réseau et y deployer des conteneurs

---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)

# Docker 101

- ***Origine de Docker***
- ***L'architecture de Docker***
## > Docker Compose
- **Docker Swarm**
- **Bonnes pratiques**

---

# Docker compose

Pour les applications utilisant plusieurs outils (front-end, backend, ...)

- Construire des applications "multi-conteneur"
- Les conteneurs fonctionnent ensemble pour former une stack.
- Espace de nom entre les conteneur d'une même stack : interaction simplifiée
- Réseau dédié : isolation
- Une seule commande pour tout gérer (simple)


---
# docker-compose.yml

- Fichier par défaut pour docker-compose : ***docker-compose.yml***
- Chaque conteneur est appelé "**service**"
    - Utilise des images existantes,
    - Peut builder des images à partir d'un Dockerfile
- Toutes les options disponibles en ligne de commande peuvent être renseignées
- Fichier au format yaml : peut-être géré en version

*[https://docs.docker.com/compose/compose-file/compose-file-v3/](https://docs.docker.com/compose/compose-file/compose-file-v3/)*

---

![h:500 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/docker-compose.png)

*Image source [https://docs.docker.com/](https://docs.docker.com/)*

---
# Intérêt de Docker compose
- Ne plus lancer des commandes complexe, avec de nombreuses options
- Permet de lier des conteneurs simplement
- Peut gérer toutes les resources docker : réseau, volumes, ....

*[https://github.com/docker/awesome-compose](https://github.com/docker/awesome-compose)*

---
# Principales commande avec docker-compose

`docker compose COMMAND`  (remplace la commande docker-compose. docker-compose est désormais un plugin de Docker)

- `up` : Crée les ressources (conteneurs, networks, images, volumes) et démarre les services.
- `start`, `stop` : arrêt ou lance un service
- `down` : Arrête et supprime les services
- `logs` : affiche les logs
- `ls` :  liste les services délployés par la stack
- `ps` :  liste les conteneur lancés par la stack
- `help`, `inspect`,  ...

*[https://docs.docker.com/engine/reference/commandline/compose/](https://docs.docker.com/engine/reference/commandline/compose/)*


---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "Docker-compose"
- Créer une stack
- Lancer/supprimer une stack

---
<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)

- ***Origine de Docker***
- ***L'architecture de Docker***
- ***Docker Compose***
## >  Docker Swarm
- **Bonnes pratiques**

---


# Docker Swarm

***Objectif :*** **Créer des clusters de machines exécutant des conteneurs Docker et fonctionnant ensemble comme une seule machine.**

- **Simplifier** au maximum le déploiement et l’administration d’un cluster
- Des fonctionnalités limitées, mais suffisantes
- Une **surcouche** à Docker et Docker Compose

---
# Cluster Swarm

- Système Distribué : **Manager(s)** / **Worker(s)**
- Tout type de machine : physique, VM, cloud, ...
- Pas de déploiement réseau spécifique préalable
- **Manager Nodes** : Noeuds chargés de la gestion du cluster
- **Worker Nodes** : Noeuds chargés d'exécuter la charge de travail

---
# Cluster Swarm

![h:400 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/swarm-diagram.png)

*[https://docs.docker.com/engine/swarm/how-swarm-mode-works/nodes/](https://docs.docker.com/engine/swarm/how-swarm-mode-works/nodes/)*

---
# Déploiement de services
![bg left:40% 95%](img/swarm-service-diagram.png)

- Description de l'état désiré pour une application
- Notion de "réplica"
- Gestion de ressources
- Définition des tâches à exécuter
  - Un conteneur Docker
  - Commandes et paramétrage


---

# Docker Swarm Configuration

Utilisation du même format de description YAML que docker-compose
Quelques champs spécifiques : 

```yaml
services:

  memcached:
    image: memcached

    deploy:
      mode: replicated
      replicas: 3
      restart_policy:
          condition: any
      placement:
          max_replicas_per_node: 1
          constraints: [node.role == worker]
```
---
# Principales commande avec docker-compose

- `docker swarm init`:  Gestion du cluster Swarm 
  - *init, join, leave, ...*
- `docker service COMMAND`  : Gestion des services
  - *create, update, ls, rm, ...*
- `docker stack COMMAND`  : Gestion des stacks
  - *deploy, ls, ps, rm, ...*

*[https://docs.docker.com/engine/reference/commandline/swarm/](https://docs.docker.com/engine/reference/commandline/swarm/)*
*[https://docs.docker.com/engine/reference/commandline/service/](https://docs.docker.com/engine/reference/commandline/service/)*
*[https://docs.docker.com/engine/reference/commandline/stack/](https://docs.docker.com/engine/reference/commandline/stack/)*

---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)

# Docker 101

- ***Origine de Docker***
- ***L'architecture de Docker***
- ***Docker Compose***
- ***Docker Swarm***
## > Bonnes pratiques

---

# Connexion aux conteneurs

### Pas de ssh
- si installation de ssh nécessaire dans un conteneur, c'est qu'on n'est pas dans la bonne voie ...
  
### Connexion en mode shell : 
- Uniquement pour des taches de mise au point/test
- Toutes les installations/configurations doivent être faites via le dockerfile

### Utilisateur de connexion
- Pas d'accès root

---
## Gestion des conteneurs

### docker run
- Utiliser l'option "-rm" :  détruire automatiquement les conteneurs quand ils ne sont plus utilisés 
- Utiliser l'option "--name" : garder la maîtrise sur ce qui est déployé

## Dockerfile

- Limiter le nombre de "COMMANDES" Docker, pour limiter le nombre de couches.

---

<!-- _backgroundColor: #2496ed -->
<!-- _color: white -->

![bg left:30% 80%](img/docker.png)

# Ressources

---

- [https://fr.tuto.com/docker/](https://fr.tuto.com/docker/)
- [https://www.udemy.com/course/debuter-avec-docker/](https://www.udemy.com/course/debuter-avec-docker/)
- [https://itnext.io/getting-started-with-docker-facts-you-should-know-d000e5815598](https://itnext.io/getting-started-with-docker-facts-you-should-know-d000e5815598)
- [https://www.redhat.com/fr/topics/conteneurs/what-is-docker](https://www.redhat.com/fr/topics/conteneurs/what-is-docker)
- [https://www.docker.com/blog/conteneurized-python-development-part-2/](https://www.docker.com/blog/conteneurized-python-development-part-2/)


## Image source : 
- [Docker] : [https://docs.docker.com/](https://docs.docker.com/)




